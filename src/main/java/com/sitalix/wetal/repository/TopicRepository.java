package com.sitalix.wetal.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.sitalix.wetal.domain.Topic;

public interface TopicRepository extends MongoRepository<Topic, String> 
{
	Topic findByLabel(String label);
	
	List<Topic> findByUserId(String userId);
}
