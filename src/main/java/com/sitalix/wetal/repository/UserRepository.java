package com.sitalix.wetal.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.sitalix.wetal.domain.User;


public interface UserRepository extends MongoRepository<User, String> 
{
	User findByFirstName(String firstName);
    
	List<User> findByLastName(String lastName);
}
