package com.sitalix.wetal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.sitalix.wetal.domain.User;
import com.sitalix.wetal.repository.UserRepository;

@Component
public class UserFillerService 
{
	
	@Autowired
	private UserRepository userRepo;

	
	public UserRepository getUserRepo() 
	{
		return userRepo;
	}

	
	public void setUserRepo(UserRepository userRepo) 
	{
		this.userRepo = userRepo;
	}
	
	
	public void fillDbWithUser(User toInsert)
	{
		userRepo.save(toInsert);
	}
	
	
}
