package com.sitalix.wetal.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TopicNotFoundException extends RuntimeException 
{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -8361791439386647480L;
	

	public TopicNotFoundException(String topicId)
	{
		super("Could not find topic with id " + topicId + " .");
	}
}
