package com.sitalix.wetal.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TopicNotBelongToUserException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 956194967095284823L;
	
	
	public TopicNotBelongToUserException(String topicId, String userId)
	{
		super("The topic with id " + topicId + " not belong to the user with id " + userId + " .");
	}

}
