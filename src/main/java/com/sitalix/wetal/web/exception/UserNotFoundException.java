package com.sitalix.wetal.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6229545259345390407L;
	

	public UserNotFoundException(String userId)
	{
		super("Could not find user with id " + userId + " .");
	}

}
