package com.sitalix.wetal.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sitalix.wetal.domain.Topic;
import com.sitalix.wetal.domain.User;
import com.sitalix.wetal.repository.TopicRepository;
import com.sitalix.wetal.repository.UserRepository;
import com.sitalix.wetal.web.exception.TopicNotBelongToUserException;
import com.sitalix.wetal.web.exception.TopicNotFoundException;
import com.sitalix.wetal.web.exception.UserNotFoundException;


@RestController
@RequestMapping("/users")
public class UserController 
{
	
	@Autowired
	private UserRepository userRepository;
	
	
	@Autowired
	private TopicRepository topicRepository;


	public void setUserRepository(UserRepository userRepository) 
	{
		this.userRepository = userRepository;
	}
	

	public void setTopicRepository(TopicRepository topicRepository) 
	{
		this.topicRepository = topicRepository;
	}


	@RequestMapping(method = RequestMethod.GET)
    public List<User> getAllUsers() 
    {
		return userRepository.findAll(); 
    }
	
	
	
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public User getUserById(@PathVariable String userId)
	{
		User user = userRepository.findOne(userId);
		
		if(user == null)
		{
			throw new UserNotFoundException(userId);
		}
		
		return user;			
    }
	
	
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public User create(@RequestBody User user) 
	{
		return userRepository.save(user);
	}
	
	
	@RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
    public User updateUser(@PathVariable String userId, @RequestBody User user) 
	{	
		User userToUpdate = userRepository.findOne(userId);
		
		if(userToUpdate == null)
		{
			throw new UserNotFoundException(userId);
		}
		
		user.setId(userToUpdate.getId());
		
		return userRepository.save(user);
	}
	
	@RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable String userId) 
	{	
		User userToDelete = userRepository.findOne(userId);
		
		if(userToDelete == null)
		{
			throw new UserNotFoundException(userId);
		}
		
		userRepository.delete(userToDelete);
	}
	
	
	@RequestMapping(value = "/{userId}/topics", method = RequestMethod.GET)
    public List<Topic> getAllUserTopics(@PathVariable String userId) 
    {
		return topicRepository.findByUserId(userId); 
    }
	
	
	@RequestMapping(value = "/{userId}/topics/{topicId}", method = RequestMethod.GET)
    public Topic getAllUserTopics(@PathVariable String userId, @PathVariable String topicId) 
    {
		Topic topic = topicRepository.findOne(topicId);
		
		if(topic == null)
		{
			throw new TopicNotFoundException(topicId);
		}
		
		if(!topic.getUserId().equalsIgnoreCase(userId))
		{
			throw new TopicNotBelongToUserException(topicId, userId);
		}
		
		return topic; 
    }
}
