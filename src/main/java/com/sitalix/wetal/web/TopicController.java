package com.sitalix.wetal.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sitalix.wetal.domain.Topic;
import com.sitalix.wetal.repository.TopicRepository;
import com.sitalix.wetal.web.exception.TopicNotFoundException;
import com.sitalix.wetal.web.exception.UserNotFoundException;

@RestController
@RequestMapping("/topics")
public class TopicController 
{
	
	@Autowired
	private TopicRepository topicRepository;

	
	public void setTopicRepository(TopicRepository topicRepository) 
	{
		this.topicRepository = topicRepository;
	}
	
	
	@RequestMapping(method = RequestMethod.GET)
    public List<Topic> getAllTopics() 
    {
		return topicRepository.findAll(); 
    }
	
	
	@RequestMapping(value = "/{topicId}", method = RequestMethod.GET)
    public Topic getTopicById(@PathVariable String topicId)
	{
		Topic topic = topicRepository.findOne(topicId);
		
		if(topic == null)
		{
			throw new TopicNotFoundException(topicId);
		}
		
		return topic;			
    }
	
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Topic create(@RequestBody Topic topic) 
	{
		return topicRepository.save(topic);
	}
	
	
	@RequestMapping(value = "/{topicId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
    public Topic updateUser(@PathVariable String topicId, @RequestBody Topic topic) 
	{	
		Topic topicToUpdate = topicRepository.findOne(topicId);
		
		if(topicToUpdate == null)
		{
			throw new TopicNotFoundException(topicId);
		}
		
		topic.setId(topicToUpdate.getId());
		
		return topicRepository.save(topic);
	}
	
	
	@RequestMapping(value = "/{topicId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable String topicId) 
	{	
		Topic topicToDelete = topicRepository.findOne(topicId);
		
		if(topicToDelete == null)
		{
			throw new UserNotFoundException(topicId);
		}
		
		topicRepository.delete(topicToDelete);
	}
	
	
}
