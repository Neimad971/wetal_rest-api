package com.sitalix.wetal.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class UserParams 
{

	@Field("languageToImprove")
	private List<String> languageToImprove;
	
	
	@Field("followedCountries")
	private List<String> followedCountries;
	
	
	@Field("followedCategories")
	private List<String> followedCategories;
	
	
	public List<String> getLanguageToImprove() 
	{
		return languageToImprove;
	}

	
	public void setLanguageToImprove(List<String> languageToImprove) 
	{
		this.languageToImprove = languageToImprove;
	}

	
	public List<String> getFollowedCountries() 
	{
		return followedCountries;
	}
	
	
	public void setFollowedCountries(List<String> followedCountries) 
	{
		this.followedCountries = followedCountries;
	}
	
	
	public List<String> getFollowedCategories() 
	{
		return followedCategories;
	}
	
	
	public void setFollowedCategories(List<String> followedCategories) 
	{
		this.followedCategories = followedCategories;
	}
}