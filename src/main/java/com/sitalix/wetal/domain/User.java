package com.sitalix.wetal.domain;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document
public class User 
{
	
	@Id
	@Field("id")
	private String id;
	
	
	@Indexed
	@Field("firstname")
	private String firstName;
	
	
	@Indexed
	@Field("lastName")
	private String lastName;
	
	
	@Indexed
	@Field("nickName")
	private String nickName;
	
	
	@Field("picture")
	private String picture;
	
	
	//@Field("video")
	//private String video;
	
	
	@Field("email")
	private String email;
	
	
	@Field("birthday")
	private DateTime birthday;
	
	
	@Field("gender")
	private String gender;
	
	
	@Field("profile")
	private String profile;
	
	
	@Field("country")
	private String country;
	
	
	@Field("spokenLanguages")
	private List<String> spokenLanguages;
	
	@Field("userParams")
	private UserParams userParams;
	
	
	public String getId() 
	{
		return id;
	}
	
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	
	public String getFirstName() 
	{
		return firstName;
	}
	
	
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}
	
	
	public String getLastName() 
	{
		return lastName;
	}
	
	
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}
	
	
	public String getNickName() 
	{
		return nickName;
	}
	
	
	public void setNickName(String nickName) 
	{
		this.nickName = nickName;
	}
	
	
	public String getPicture() 
	{
		return picture;
	}
	
	
	public void setPicture(String picture) 
	{
		this.picture = picture;
	}
	
	
	/*public String getVideo() 
	{
		return video;
	}
	
	
	public void setVideo(String video) 
	{
		this.video = video;
	}*/
	
	
	public String getEmail() 
	{
		return email;
	}
	
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	
	public DateTime getBirthday() 
	{
		return birthday;
	}
	
	
	public void setBirthday(DateTime birthday) 
	{
		this.birthday = birthday;
	}
	
	
	public String getGender() 
	{
		return gender;
	}
	
	
	public void setGender(String gender) 
	{
		this.gender = gender;
	}
	
	
	public String getProfile() 
	{
		return profile;
	}
	
	
	public void setProfile(String profile) 
	{
		this.profile = profile;
	}
	
	
	public String getCountry() 
	{
		return country;
	}
	
	
	public void setCountry(String country) 
	{
		this.country = country;
	}
	
	
	public List<String> getSpokenLanguages() 
	{
		return spokenLanguages;
	}

	
	public void setSpokenLanguages(List<String> spokenLanguages) 
	{
		this.spokenLanguages = spokenLanguages;
	}

	
	public UserParams getUserParams() 
	{
		return userParams;
	}
	
	
	public void setUserParams(UserParams userParams) 
	{
		this.userParams = userParams;
	}
}