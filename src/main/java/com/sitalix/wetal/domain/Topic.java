package com.sitalix.wetal.domain;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Topic 
{
	
	@Id
	@Field("id")
	private String id;
	
	
	@Indexed
	@Field("label")
	private String label;
	
	
	@Field("description")
	private String description;
	
	
	@Field("duration")
	private int duration;
	
	
	@Field("appointmentDate")
	private DateTime appointmentDate;
	
	
	@Field("whateverTopic")
	private boolean whateverTopic;
	
	
	@Field("categoryId")
	private String categoryId;
	
	
	//@CreatedBy
	@Field("userId")
	private String userId;
	
	
	//@CreatedDate
	//@Field("createdAt")
	//private DateTime createdAt;
	 
	
	public String getId() 
	{
		return id;
	}
	
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	
	public String getLabel() 
	{
		return label;
	}
	
	
	public void setLabel(String label) 
	{
		this.label = label;
	}
	
	
	public String getDescription() 
	{
		return description;
	}
	
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	
	public int getDuration() 
	{
		return duration;
	}
	
	
	public void setDuration(int duration) 
	{
		this.duration = duration;
	}
	
	
	public DateTime getAppointmentDate() 
	{
		return appointmentDate;
	}
	
	
	public void setAppointmentDate(DateTime appointmentDate) 
	{
		this.appointmentDate = appointmentDate;
	}
	
	
	public boolean isWhateverTopic() 
	{
		return whateverTopic;
	}
	
	
	public void setWhateverTopic(boolean whateverTopic) 
	{
		this.whateverTopic = whateverTopic;
	}
	
	
	public String getCategoryId() 
	{
		return categoryId;
	}
	
	
	public void setCategoryId(String categoryId) 
	{
		this.categoryId = categoryId;
	}
	
	
	public String getUserId() 
	{
		return userId;
	}
	
	
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}
}