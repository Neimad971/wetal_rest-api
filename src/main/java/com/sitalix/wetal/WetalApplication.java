package com.sitalix.wetal;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication // same as @Configuration, @EnableAutoConfiguration, @ComponentScan
public class WetalApplication 
{
	

	public static void main(String[] args) 
	{
		SpringApplication.run(WetalApplication.class, args);
    }
}
