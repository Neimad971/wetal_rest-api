package com.sitalix.wetal.web;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.sitalix.wetal.domain.Topic;
import com.sitalix.wetal.repository.TopicRepository;


public class TopicControllerTest extends CommonControllerTest
{
	
	
	private List<Topic> topicList;
	    
	    
	@Autowired
	private TopicRepository topicRepository;
	
	
	public void setTopicRepository(TopicRepository topicRepository)
	{
		this.topicRepository = topicRepository;
	}


	@Override
    @Before
    public void setup() throws Exception 
    {
        super.setup();
        
        topicRepository.deleteAll();
        
        topicList = newArrayList();
        
        Topic topic1 = new Topic();
        topic1.setId("1");
        topic1.setLabel("Classico Madrid-Barca");
        topic1.setDescription("about refereeing");   	
    	topic1.setDuration(390);
    	//topic1.setAppointmentDate(new DateTime());
    	topic1.setWhateverTopic(false);
    	topic1.setCategoryId("2");
    	topic1.setUserId("a3d5n");
    	
    	
    	Topic topic2 = new Topic();
    	topic2.setId("2");
    	topic2.setLabel("Night run movie");
    	topic2.setDescription("about characters");   	
    	topic2.setDuration(938);
      	//topic2.setAppointmentDate(new DateTime());
    	topic2.setWhateverTopic(false);
    	topic2.setCategoryId("9");
    	topic2.setUserId("9f3g4");
    	
    	
    	Topic topic3 = new Topic();
    	topic3.setId("3");
    	topic3.setLabel("Orlando flood");
    	topic3.setDescription("about damages");   	
    	topic3.setDuration(637);
      	//topic3.setAppointmentDate(new DateTime());
    	topic3.setWhateverTopic(false);
    	topic3.setCategoryId("5");
    	topic3.setUserId("0t8b6");
    	
    	
    	topicList.add(topic1);
    	topicList.add(topic2);
    	topicList.add(topic3);
    	
    	topicRepository.save(topicList.get(0));
    	topicRepository.save(topicList.get(1));
    	topicRepository.save(topicList.get(2));
    	
    	
    }
	
	
	@Test
    public void readAllTopics() throws Exception
    {
		
		mockMvc.perform(get("/topics")
        		.content(this.json(new Topic()))
        		.contentType(contentType))
        		.andExpect(status().isOk())
        		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(3)))
        		.andExpect(MockMvcResultMatchers.jsonPath("$[0].id", is(this.topicList.get(0).getId())))
        		.andExpect(MockMvcResultMatchers.jsonPath("$[0].label", is(this.topicList.get(0).getLabel())))
        		.andExpect(MockMvcResultMatchers.jsonPath("$[0].description", is(this.topicList.get(0).getDescription())))
        		.andExpect(MockMvcResultMatchers.jsonPath("$[0].duration", is(this.topicList.get(0).getDuration())))
        		.andExpect(MockMvcResultMatchers.jsonPath("$[0].whateverTopic", is(this.topicList.get(0).isWhateverTopic())))
        		.andExpect(MockMvcResultMatchers.jsonPath("$[0].categoryId", is(this.topicList.get(0).getCategoryId())))
        		.andExpect(MockMvcResultMatchers.jsonPath("$[0].userId", is(this.topicList.get(0).getUserId())))
		
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].id", is(this.topicList.get(1).getId())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].label", is(this.topicList.get(1).getLabel())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].description", is(this.topicList.get(1).getDescription())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].duration", is(this.topicList.get(1).getDuration())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].whateverTopic", is(this.topicList.get(1).isWhateverTopic())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].categoryId", is(this.topicList.get(1).getCategoryId())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].userId", is(this.topicList.get(1).getUserId())))
				
				.andExpect(MockMvcResultMatchers.jsonPath("$[2].id", is(this.topicList.get(2).getId())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[2].label", is(this.topicList.get(2).getLabel())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[2].description", is(this.topicList.get(2).getDescription())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[2].duration", is(this.topicList.get(2).getDuration())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[2].whateverTopic", is(this.topicList.get(2).isWhateverTopic())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[2].categoryId", is(this.topicList.get(2).getCategoryId())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[2].userId", is(this.topicList.get(2).getUserId()))); 	
    }
	
	
	@Test
    public void readOneTopic() throws Exception
    {
    	mockMvc.perform(get("/topics/" + topicList.get(0).getId())
        		.content(this.json(new Topic()))
        		.contentType(contentType))
        		.andExpect(status().isOk())
        		.andExpect(MockMvcResultMatchers.jsonPath("$.id", is(this.topicList.get(0).getId())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.label", is(this.topicList.get(0).getLabel())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.description", is(this.topicList.get(0).getDescription())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.duration", is(this.topicList.get(0).getDuration())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.whateverTopic", is(this.topicList.get(0).isWhateverTopic())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.categoryId", is(this.topicList.get(0).getCategoryId())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.userId", is(this.topicList.get(0).getUserId())));
    }
	
	
	@Test
    public void topicNotFound() throws Exception 
    {
        mockMvc.perform(get("/topics/4")
                .content(this.json(new Topic()))
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }
	
	
	@Test
    public void createTopic() throws Exception 
    {
		Topic topic = new Topic();
        topic.setId("19");
        topic.setLabel("Free topic");
        //no description   	
    	topic.setDuration(833);
    	//topic.setAppointmentDate(new DateTime());
    	topic.setWhateverTopic(true);
    	topic.setCategoryId("7");
    	topic.setUserId("3l7e2");
    	
    	String topicJson = json(topic);
        
        this.mockMvc.perform(post("/topics")
                .contentType(contentType)
                .content(topicJson))
                .andExpect(status().isCreated());
    }
	
	
	@Test
    public void updateTopic() throws Exception 
    {
    	Topic topic = topicList.get(0);
    	topic.setDescription("newDescription");
    	String userJson = json(topic);
    	
        this.mockMvc.perform(put("/topics/" + topicList.get(0).getId())
                .contentType(contentType)
                .content(userJson))
                .andExpect(status().isOk());
    }
	
	
	@Test
    public void deleteTopic() throws Exception 
    {
		Topic topic = topicList.get(1);
    	String userJson = json(topic);
    	
        this.mockMvc.perform(put("/topics/" + topicList.get(1).getId())
                .contentType(contentType)
                .content(userJson))
                .andExpect(status().isOk());
    }
}
